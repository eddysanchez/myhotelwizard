package Controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

public class FileWrite {
	private static PrintWriter pw;
	private static String deskPath = System.getProperty("user.home")+"/Desktop";

	private FileWrite() {
	}

	public static void createFile(String filename, String toWrite) {
		try {
			File desktop = new File(deskPath, "["+LocalDate.now().toString()+"]"+filename);
			if (!desktop.exists()) {
				desktop.createNewFile();
			}
			pw = new PrintWriter(new BufferedWriter(new FileWriter(desktop)));
			pw.write(toWrite);
			pw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
