package model;

import java.time.LocalDate;
import java.util.Calendar;

public class Guest {
	private final String name;
	private final String surname;
	private final String CF;
	private final LocalDate birth;

	public Guest(String name, String surname, String CF, LocalDate birth) {
		this.name = name;
		this.surname = surname;
		this.CF = CF;
		this.birth = birth;
	}

	public String getName() {
		return new String(this.name);
	}

	public String getSurname() {
		return new String(this.surname);
	}

	public String getCF() {
		return new String(this.CF);
	}

	public LocalDate getBirthDate() {
		return this.birth;
	}

	public int getAge() {
		return Calendar.getInstance().get(Calendar.YEAR) - birth.getYear();
	}

	public String toString() {
		return "[GUEST]: " + this.getName() + " " + this.getSurname() + "\n[BIRTH, CF]: " + this.getBirthDate() + " "
				+ this.getCF();
	}
}
