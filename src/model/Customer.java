package model;

import java.time.LocalDate;

public class Customer extends Guest {
	private Booking booking;

	public Customer(String name, String surname, String CF, LocalDate birth, Booking booking) {
		super(name, surname, CF, birth);
		this.booking = booking;
	}

	public Booking getBooking() {
		return this.booking;
	}

	public String toString() {
		return "[CUSTOMER]: " + this.getName() + " " + this.getSurname() + "\n[BIRTH, CF]: " + this.getBirthDate()
				+ " " + this.getCF();
	}
	
	public String toStringTotal() {
		return this.toString()+"\n"+this.getBooking().toString();
	}
}
