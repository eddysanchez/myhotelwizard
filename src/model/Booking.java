package model;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import model.interfaces.IBooking;

public class Booking implements IBooking{
	private Room room;
	private LocalDate startDate;
	private LocalDate endDate;
	private final BookingType bType;
	private final SeasonType sType;
	private Optional<List<Guest>> guestsList = Optional.empty();
	private Optional<Set<Extra>> extraList = Optional.empty();
	private Catalog catalog;
	private double price;
	private double payed;
	private boolean hasEaten;
	private LocalDate today;

	public Booking(BookingType bType, SeasonType sType, Catalog catalog, LocalDate startDate, LocalDate endDate,
			Double caparra) {
		this.bType = bType;
		this.sType = sType;
		this.price = calculatePrice();
		this.startDate = startDate;
		this.endDate = endDate;
		this.payed = caparra;
		this.hasEaten = false;
		this.today = LocalDate.now();
		canEat();
	}

	public Room getRoom() {
		return this.room;
	}
	
	

	public Double getPrice() {
		return this.price;
	}

	public LocalDate getStartDate() {
		return this.startDate;
	}

	public LocalDate getEndDate() {
		return this.endDate;
	}

	public List<Guest> getGuestsList() {
		return new ArrayList<Guest>(this.guestsList.get());
	}
	
	public BookingType getBookingType() {
		return this.bType;
	}

	public void setPrice(double price) {
		this.price = price; //
	}

	public void setStartDate(LocalDate date) {
		this.startDate = date;
		this.setPrice(calculatePrice());
	}

	public void setEndDate(LocalDate date) {
		this.startDate = date;
		this.setPrice(calculatePrice());
	}

	// WARNING, THIS IS THE DAILY PRICE!!!

	public boolean hasPayed() {
		return (this.payed == this.price);
	}

	public double getToPay() {
		return this.price - this.payed;
	}

	public void changeAmount(double amount) {
		this.price += amount;
	}

	public void pay(double amount) {
		this.payed = amount;
	}

	public void addExtra(Extra extra) {
		this.extraList.get().add(extra);
		this.changeAmount(extra.getCost());

	}
	
	public void updateEatenDate() {
		if(!today.equals(LocalDate.now())) {
			this.hasEaten = false;
			this.today = LocalDate.now();
		}
	}
	
	public void eat() {
		this.hasEaten = true;
	}
	
	public boolean canEat() {
		if(this.bType.equals(BookingType.BB) || this.bType.equals(BookingType.OVERNIGHT)) {
			return false;
		} else {
			if(bType.equals(BookingType.HALFBOARD) && this.hasEaten == true) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	public boolean hasEatenToday() {
		return this.hasEaten;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(this.room + "\n[BOOKING START DATE/END DATE]: "
				+ this.getStartDate().toString() + "   " + this.getEndDate().toString() + "\n[PAYEMENT/TO PAY]: "
				+ this.getPrice() + "/" + this.getToPay() + "\n[GUESTS INFOS]: ");
		for (Guest g : this.getGuestsList()) {
			sb.append(g.toString() + "\n");
		}
		sb.append("[EXTRAS]: ");
		if (this.extraList.isPresent()) {
			for (Extra e : this.extraList.get()) {
				sb.append(e.toString());
			}
		}
		return new String(sb);
	}

	private double calculatePrice() {
		double price = 0;
		if (sType.equals(SeasonType.LOWSEASON)) {
			price += catalog.getLow();
		} else if (sType.equals(SeasonType.MIDSEASON)) {
			price += catalog.getMid();
		} else {
			price += catalog.getHigh();
		}
		if (bType.equals(BookingType.BB)) {
			price *= catalog.getBBOverPrice();
		} else if (bType.equals(BookingType.HALFBOARD)) {
			price *= catalog.getHalfBoardOverPrice();
		} else if (bType.equals(BookingType.FULLBOARD)) {
			price *= catalog.getFullBoardOverPrice();
		}
		// daily price for adult, which is the customer
		for (Guest g : guestsList.get()) {
			if (g.getAge() < catalog.getBabyAge()) {
				// do-nothing
			} else if (g.getAge() < catalog.getChildAge()) {
				price += (price * catalog.getChildPercentage());
			} else
				price += price;
		}
		// ending price counting the journeydays;
		price *= this.getJourneyDays();
		return price;
	}
	
	private int getJourneyDays() {
		return Period.between(startDate, endDate).getDays();
	}
}
