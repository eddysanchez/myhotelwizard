package model.interfaces;

import java.time.LocalDate;
import java.util.List;

import model.Extra;
import model.Guest;
import model.Room;

public interface IBooking {
	
	public Room getRoom();
	
	public Double getPrice();
	
	public LocalDate getStartDate();
	
	public LocalDate getEndDate();
	
	public List<Guest> getGuestsList();
	
	public void setPrice(double price);
	
	public void setStartDate(LocalDate date);
	
	public void setEndDate(LocalDate date);
	
	public boolean hasPayed();
	
	public double getToPay();
	
	public void changeAmount(double amount);
	
	public void pay(double amount);
	
	public void addExtra(Extra extra);
	
	public String toString();

}
