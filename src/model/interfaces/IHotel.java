package model.interfaces;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

import model.Booking;
import model.Extra;
import model.Guest;
import model.Room;

public interface IHotel {

	public String getName();

	public HashSet<Guest> getCustomerList();

	public List<Booking> getBookingList();

	public List<Room> getRoomList();

	public void setName(String name);

	public void addGuest(Guest guest);

	public void removeGuest(Guest guest);

	public void addRoom(Room room);

	public void removeRoom(Room room);

	public void addBooking(Booking booking);

	public void removeBooking(Booking booking);
	
	public void addExtra(Extra extra);
	
	public void removeExtra(Extra extra);
	
	public void addBalanceVoice(String voiceOfChange, double amount);
	
	public double getActualBalance();
	
	public List<Guest> findSomeone(String name, String surname);
	
	public List<Guest> findSomeone(LocalDate begin, LocalDate end);
	
	public List<Guest> findSomeone(String surname);

	public String balanceVoiceToString(String voice, double amount);
	
	public String dailyBalanceVoices(LocalDate date);
	
	public String intervalBalanceVoices(LocalDate start, LocalDate end);
	
	public String totalBalanceVoices();
	

}
