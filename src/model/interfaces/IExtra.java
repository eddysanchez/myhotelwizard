package model.interfaces;

import model.Extra;

public interface IExtra {

	public Double getCost();

	public String getName();

	public Extra getExtra();

	public void setCost(double cost);

	public void setName(String name);

	public String toString();

}
