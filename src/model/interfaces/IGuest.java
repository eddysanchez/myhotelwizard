package model.interfaces;

public interface IGuest {
	public String getName();

	public String getSurname();

	public String getUserName();

	public String getPass();

}
