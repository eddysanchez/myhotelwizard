package model.interfaces;

public interface IDailyMenu {

	public void addStarter(String starter);

	public void deleteStarter(String starter);

	public void addFirstPlate(String firstPlate);

	public void deleteFirstPlate(String firstPlate);

	public void addSecondPlate(String secondPlate);

	public void deleteSecondPlate(String secondPlate);

	public void addDessert(String dessert);

	public void deleteDessert(String dessert);

	public void cleanAll();

}
