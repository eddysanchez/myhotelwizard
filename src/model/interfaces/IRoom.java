package model.interfaces;

import model.Customer;
import model.RoomType;

public interface IRoom {

	public Integer getNumber();

	public Integer getMaxGuestsNumber();

	public RoomType getType();

	public Customer getActualCustomer();

	public void setActualCustomer(Customer customer);

	public void deleteActualCustomer();

	public boolean isBusy();

}
