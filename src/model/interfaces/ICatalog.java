package model.interfaces;

public interface ICatalog {

	public Double getLow();

	public Double getMid();

	public Double getHigh();

	public Double getBBOverPrice();

	public Double getHalfBoardOverPrice();

	public Double getFullBoardOverPrice();

	public Integer getChildAge();

	public Integer getBabyAge();

	public Double getChildPercentage();

	public void setLow(double dayLow);

	public void setMid(double dayMid);

	public void setHigh(double dayHigh);

	public void setBBOverPrice(double BBOverPrice);

	public void setHalfBoardOverPrice(double halfBoardOverPrice);

	public void setFullBoardOverPrice(double fullBoardOverPrice);

	public void setChildAge(int childAge);

	public void setBabyAge(int babyAge);

	public void setChildPercentage(double childPercentage);

}
