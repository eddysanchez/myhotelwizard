package model;

import model.interfaces.IExtra;

public class Extra implements IExtra{

	private String extraName;
	private double extraCost;

	public Extra(String name, double cost) {
		this.extraName = name;
		this.extraCost = cost;
	}

	public Double getCost() {
		return new Double(this.extraCost);
	}

	public String getName() {
		return new String(this.extraName);
	}

	public Extra getExtra() {
		return new Extra(this.getName(), this.getCost());
	}

	public void setCost(double cost) {
		this.extraCost = cost;
	}

	public void setName(String name) {
		this.extraName = name;
	}
	
	public String toString() {
		return this.getName()+ " [COSTO]"+this.getCost(); 
	}

}
