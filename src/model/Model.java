package model;

public class Model {
	private static Model myInstance;
	private Hotel hotel;
	// private View view;
	// private Controller observer;
	
	private Model() {

	}

	private Model(Hotel hotel) {
		this.hotel = hotel;
	}

	public Hotel getHotel() {
		return this.hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/*
	 * public void setView(View view) { this.view = view; }
	 * 
	 * public View getView() { return this.view}
	 * 
	 * public void setController(Controller controller) { this.observer =
	 * observer; }
	 * 
	 * public Controller getController() { return this.observer; }
	 * 
	 * 
	 */

	public static Model getInstance() {
		if (myInstance == null) {
			myInstance = new Model();
		}
		return myInstance;
	}

	public static Model getInstance(Hotel hotel) {
		if (myInstance == null) {
			myInstance = new Model(hotel);
		}
		return myInstance;
	}

}
