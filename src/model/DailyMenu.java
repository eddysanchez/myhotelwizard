package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DailyMenu {
	private static DailyMenu instance;
	private List<String> starters;
	private List<String> firstPlates;
	private List<String> secondPlates;
	private List<String> desserts;
	private HashMap<Room, Boolean> allRooms;
	private StringBuilder sb;
	public final static String fileNameMenu = "Menu.txt";
	public final static String fileNameOrders = "Ordini.txt";

	private DailyMenu(List<Room> allRooms) {
		this.starters = new ArrayList<String>();
		this.firstPlates = new ArrayList<String>();
		this.secondPlates = new ArrayList<String>();
		this.desserts = new ArrayList<String>();
		this.allRooms = new HashMap<>();
		computeRoomMealAvaliable(allRooms);
		sb = new StringBuilder("[ORDINI]: ");
	}

	public void addStarter(String starter) {
		this.starters.add(starter);
	}

	public void deleteStarter(String starter) {
		this.starters.remove(starter);
	}

	public void addFirstPlate(String firstPlate) {
		this.firstPlates.add(firstPlate);
	}

	public void deleteFirstPlate(String firstPlate) {
		this.firstPlates.remove(firstPlate);
	}

	public void addSecondPlate(String secondPlate) {
		this.secondPlates.add(secondPlate);
	}

	public void deleteSecondPlate(String secondPlate) {
		this.secondPlates.remove(secondPlate);
	}

	public void addDessert(String dessert) {
		this.desserts.add(dessert);
	}

	public void removeDessert(String dessert) {
		this.desserts.remove(dessert);
	}

	public void cleanAllandUpdate() {
		this.starters.clear();
		this.firstPlates.clear();
		this.secondPlates.clear();
		this.desserts.clear();
		sb = new StringBuilder("[ORDINI]: ");
	}

	private void addRoomMeal(Room room, List<String> starter, List<String> first, List<String> second,
			List<String> dessert) {
		room.getActualCustomer().getBooking().eat();
		sb.append("[STANZA " + room.getNumber() + "]\n" + "[ANTIPASTI]: \n");
		for (String s : starter) {
			int count = 0;
			for (String s1 : this.starters) {
				if (s.equals(s1))
					count++;
			}
			if (count > 0) {
				sb.append(count + " " + s + "\n");
			}
		}
		sb.append("[PRIMI]: \n");
		for (String s : first) {
			int count = 0;
			for (String s1 : this.firstPlates) {
				if (s.equals(s1))
					count++;
			}
			if (count > 0) {
				sb.append(count + " " + s + "\n");
			}
		}
		sb.append("[SECONDI]: \n");
		for (String s : second) {
			int count = 0;
			for (String s1 : this.secondPlates) {
				if (s.equals(s1))
					count++;
			}
			if (count > 0) {
				sb.append(count + " " + s + "\n");
			}
		}
		sb.append("[DOLCI]: \n");
		for (String s : dessert) {
			int count = 0;
			for (String s1 : this.desserts) {
				if (s.equals(s1))
					count++;
			}
			if (count > 0) {
				sb.append(count + " " + s + "\n");
			}
		}
		sb.append("\n\n");
	}

	public String getOrdersString() {
		return new String(sb);
	}

	public String getMenuString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[ANTIPASTI]: ");
		for (String s : this.starters) {
			sb.append(s + "\n");
		}
		sb.append("\n[PRIMI]: ");
		for (String s : this.firstPlates) {
			sb.append(s + "\n");
		}
		sb.append("\n[SECONDI]: ");
		for (String s : this.secondPlates) {
			sb.append(s + "\n");
		}
		sb.append("\n[DESSERTS]: ");
		for (String s : this.desserts) {
			sb.append(s + "\n");
		}
		return new String(sb);
	}

	public void computeRoomMealAvaliable(List<Room> allRooms) {
		for (Room r : allRooms) {
			if (!r.isBusy()) {
				this.allRooms.put(r, false);
			} else {
				this.allRooms.put(r, r.getActualCustomer().getBooking().canEat());
			}
		}
	}

	public static DailyMenu getInstance(List<Room> list) {
		if (instance == null) {
			instance = new DailyMenu(list);
		}
		return instance;
	}

}
