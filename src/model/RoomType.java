package model;

public enum RoomType {
	STANDARD, PREMIUM, SUITE;
}
