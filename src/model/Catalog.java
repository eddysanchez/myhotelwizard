package model;

import model.interfaces.ICatalog;

public class Catalog implements ICatalog{
	private static Catalog myInstance;
	private double dayLow;
	private double dayMid;
	private double dayHigh;
	private double BBOverPrice;
	private double halfBoardOverPrice;
	private double fullBoardOverPrice;
	private double childPercentage;
	private int childAge;
	private int babyAge;

	private Catalog(double dayLow, double dayMid, double dayHigh, double BBOverPrice, double halfBoardOverPrice,
			double fullBoardOverPrice, double childPercentage, int childAge, int babyAge) {
		this.dayLow = dayLow;
		this.dayMid = dayMid;
		this.dayHigh = dayHigh;
		this.BBOverPrice = BBOverPrice;
		this.halfBoardOverPrice = halfBoardOverPrice;
		this.fullBoardOverPrice = fullBoardOverPrice;
		this.childAge = childAge;
		this.childPercentage = childPercentage;
	}
	

	public Double getLow() {
		return new Double(this.dayLow);
	}

	public Double getMid() {
		return new Double(this.dayMid);
	}

	public Double getHigh() {
		return new Double(this.dayHigh);
	}

	public Double getBBOverPrice() {
		return new Double(this.BBOverPrice);
	}

	public Double getHalfBoardOverPrice() {
		return new Double(this.halfBoardOverPrice);
	}

	public Double getFullBoardOverPrice() {
		return new Double(this.fullBoardOverPrice);
	}

	public Integer getChildAge() {
		return new Integer(this.childAge);
	}

	public Integer getBabyAge() {
		return new Integer(this.babyAge);
	}

	public Double getChildPercentage() {
		return 1 - (this.childPercentage / 100);
	}

	public void setLow(double dayLow) {
		this.dayLow = dayLow;
	}

	public void setMid(double dayMid) {
		this.dayMid = dayMid;
	}

	public void setHigh(double dayHigh) {
		this.dayHigh = dayHigh;
	}

	public void setBBOverPrice(double BBOverPrice) {
		this.BBOverPrice = BBOverPrice;
	}

	public void setHalfBoardOverPrice(double halfBoardOverPrice) {
		this.halfBoardOverPrice = halfBoardOverPrice;
	}

	public void setFullBoardOverPrice(double fullBoardOverPrice) {
		this.fullBoardOverPrice = fullBoardOverPrice;
	}

	public void setChildAge(int childAge) {
		this.childAge = childAge;
	}

	public void setChildPercentage(double childPercentage) {
		this.childPercentage = childPercentage;
	}

	public void setBabyAge(int babyAge) {
		this.babyAge = babyAge;
	}

	public static Catalog getInstance(double dayLow, double dayMid, double dayHigh, double BBOverPrice,
			double halfBoardOverPrice, double fullBoardOverPrice, double childPercentage, int childAge, int babyAge) {
		if (myInstance == null) {
			myInstance = new Catalog(dayLow, dayMid, dayHigh, BBOverPrice, halfBoardOverPrice, fullBoardOverPrice,
					childPercentage, childAge, babyAge);
		}
		return myInstance;
	}

}
