package model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import model.interfaces.IHotel;

public class Hotel implements IHotel {
	private static Hotel myInstance;
	private String name;
	private List<Room> roomList;
	private HashMap<String, Guest> guestsList;
	private List<Booking> bookingList;
	private List<Extra> extraList;
	private double actualBalance;
	private HashMap<LocalDate, HashMap<String, Double>> incomingOutcoming;

	private Hotel(String name) {
		this.name = name;
		this.roomList = new ArrayList<>();
		this.guestsList = new HashMap<String, Guest>();
		this.bookingList = new ArrayList<Booking>();
		this.actualBalance = 0;
	}

	public String getName() {
		return new String(this.name);
	}

	public HashSet<Guest> getCustomerList() {
		return new HashSet<Guest>(this.guestsList.values());
	}

	public List<Room> getRoomList() {
		return new ArrayList<Room>(this.roomList);
	}

	public List<Booking> getBookingList() {
		return new ArrayList<Booking>(this.bookingList);
	}

	public List<Extra> getExtraList() {
		return new ArrayList<Extra>(this.extraList);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addGuest(Guest guest) {
		this.guestsList.put(guest.getCF(), guest);
	}

	public void removeGuest(Guest guest) {
		this.guestsList.remove(guest);
	}

	public void addRoom(Room room) {
		if (!this.roomList.contains(room)) {
			this.roomList.add(room);
		}
	}

	public void removeRoom(Room room) {
		if (this.roomList.contains(room)) {
			this.roomList.remove(room);
		}
	}

	public void addBooking(Booking booking) {
		if (!this.bookingList.contains(booking)) {
			this.bookingList.add(booking);
		}
	}

	public void removeBooking(Booking booking) {
		if (this.bookingList.contains(booking)) {
			this.bookingList.remove(booking);
		}
	}

	public void addExtra(Extra extra) {
		if (!this.extraList.contains(extra)) {
			this.extraList.add(extra);
		}
	}

	public void removeExtra(Extra extra) {
		if (this.extraList.contains(extra)) {
			this.extraList.remove(extra);
		}
	}

	public void addBalanceVoice(String voiceOfChange, double amount) {
		HashMap<String, Double> tmp = new HashMap<String, Double>();
		if (this.incomingOutcoming.containsKey(LocalDate.now())) {
			tmp = this.incomingOutcoming.get(LocalDate.now());
		}
		tmp.put(voiceOfChange, amount);
		this.incomingOutcoming.put(LocalDate.now(), tmp);
		this.updateBalance(amount);
	}


	public double getActualBalance() {
		return this.actualBalance;
	}

	public List<Guest> findSomeone(LocalDate begin, LocalDate end) {
		ArrayList<Guest> ar = new ArrayList<>();
		for (Guest g : this.guestsList.values()) {
			if (g instanceof Customer) {
				if (((Customer) g).getBooking().getStartDate().compareTo(begin) > 0
						&& ((Customer) g).getBooking().getStartDate().compareTo(end) < 0) {
					ar.add(g);
					ar.addAll(((Customer) g).getBooking().getGuestsList());
				}
			}
		}
		return ar;
	}

	public List<Guest> findSomeone(String name, String surname) {
		ArrayList<Guest> ar = new ArrayList<>();
		for (Guest g : this.guestsList.values()) {
			if (g.getName().equals(name) && g.getSurname().equals(surname)) {
				ar.add(g);
			}
		}
		return ar;
	}

	public List<Guest> findSomeone(String surname) {
		ArrayList<Guest> ar = new ArrayList<>();
		for (Guest g : this.guestsList.values()) {
			if (g.getSurname().equals(surname))
				ar.add(g);
		}
		return ar;
	}


	public String balanceVoiceToString(String voice, double amount) {
		return "[VOCE]: " + voice + "\n[IMPORTO]: " + amount + "\n";
	}

	public String dailyBalanceVoices(LocalDate date) {
		StringBuilder sb = new StringBuilder();
		sb.append("[DATA]: " + date.toString() + "\n");
		for (Map.Entry<String, Double> hm : this.incomingOutcoming.get(date).entrySet()) {
			sb.append(balanceVoiceToString(hm.getKey(), hm.getValue()));
		}
		return new String(sb);
	}

	public String intervalBalanceVoices(LocalDate start, LocalDate end) {
		StringBuilder sb = new StringBuilder();
		for (LocalDate date : this.incomingOutcoming.keySet()) {
			if (date.compareTo(start) <= 0 && date.compareTo(end) >= 0) {
				sb.append(dailyBalanceVoices(date) + "\n");
			}
		}
		return new String(sb);
	}

	public String totalBalanceVoices() {
		StringBuilder sb = new StringBuilder();
		for (LocalDate date : this.incomingOutcoming.keySet()) {
			sb.append(dailyBalanceVoices(date) + "\n");
		}
		return new String(sb);
	}
	
	public static Hotel getInstance(String name) {
		if (myInstance == null) {
			myInstance = new Hotel(name);
		}
		return myInstance;
	}
	
	private void updateBalance(double amount) {
		this.actualBalance += amount;
	}
}
