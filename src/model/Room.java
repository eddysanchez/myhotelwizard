package model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import model.interfaces.IRoom;

public class Room implements IRoom {
	private final int number;
	private RoomType type;
	private final int maxGuestsNumber;
	private Optional<List<Customer>> customersList;
	private Optional<Customer> actualCustomer;

	public Room(int number, RoomType type, int maxGuestsNumber) {
		this.number = number;
		this.type = type;
		this.actualCustomer = Optional.empty();
		this.customersList = Optional.empty();
		this.maxGuestsNumber = maxGuestsNumber;
	}

	public Integer getNumber() {
		return new Integer(this.number);
	}

	public Integer getMaxGuestsNumber() {
		return new Integer(this.maxGuestsNumber);
	}

	public RoomType getType() {
		return this.type;
	}
	
	public void addCustomer(Customer customer) {
		this.customersList.get().add(customer);
		Collections.sort(this.customersList.get(), new Comparator<Customer>() {
		    public int compare(Customer c1, Customer c2) {
		        return c1.getBooking().getStartDate().compareTo(c2.getBooking().getStartDate());
		    }
		});
	}
	
	public void deleteCustomer(Customer customer) {
		this.customersList.get().remove(customer);
	}

	public Customer getActualCustomer() {
		return this.actualCustomer.get();
	}
	
	public Customer getNextCustomer() {
		boolean next = false;
		for(Customer c: this.customersList.get()) {
			if(next == true)
				return c;
			if(this.actualCustomer.equals(c)) {
				next = true;
			}
		}
		return null;
	}

	public void setActualCustomer(Customer customer) {
		this.actualCustomer = Optional.of(customer);
	}

	public void deleteActualCustomer() {
		this.actualCustomer = Optional.empty();
	}

	public boolean isBusy() {
		return this.actualCustomer.isPresent();
	}
	
	public String toString() {
		return "[ROOM]: " +this.getNumber();
	}
}
